package com.alugaki.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alugaki.model.Veiculo;

public interface VeiculoRepository extends JpaRepository<Veiculo, Long> {

	public List<Veiculo> findByMarcaContaining(String marca);
	public List<Veiculo> findByNomeContaining(String nome);
}
